# Introduction
| item | description |
| -- | -- |
| Project name | Play SDK Basic |
| Project version | 1.0.9 |
| Release date | 2016-08-10 |
| Developer | Ruslan Aliyev |

# Function lists
- Login 
- Google Analytics
- AppsFlyer
- Google In-app Billing
- Google Cloud Messaging

# Change Log

1. Changes to the internal workings of Play SDK Basic's activities.

# Preliminary Setup

#### 1. AppsFlyer

1. Give your project an Application ID, like this: `applicationId "xxx.yyyyy.zz"`. This can be your project's package number. In your application's `build.gradle` file, described below under the heading: **'what build.gradle need to set'**.
2. Contact us, tell us what your Application ID is.
3. We will give you an AppsFlyer key.
4. You put this AppsFlyer key into project, like this: `playSDKBasic.config().setAFKey("AppsFlyer Key")`. In your first `.java` activity's `onCreate` section, described below under the heading: **'First Java Page (Before Login)'**.

#### 2. The aar file

Put playsdkbasic-1.0.9.aar into libs folder of Android Studio Project

# Setup app gradle

### what build.gradle need to set
```gradle
apply plugin: 'com.android.application'

android {
    compileSdkVersion 23
    buildToolsVersion "23.0.3"

    defaultConfig {
        applicationId "com.xxx.yyy"
        minSdkVersion 15
        targetSdkVersion 23
        versionCode 1
        versionName "1.0.0"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
}
repositories {
    flatDir {
        dirs 'libs'
    }
}
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    testCompile 'junit:junit:4.12'
    compile 'com.android.support:appcompat-v7:23.0.0'
    compile 'com.forgame.playsdkbasic:playsdkbasic-release:1.0.9@aar'
    compile 'com.google.android.gms:play-services:8.3.0'
    compile 'com.facebook.android:facebook-android-sdk:4.1.0'
}
```

# Setup Manifest
```xml
<application
	android:name="com.forgame.playsdkbasic.PlayBasicApplication"
	android:label="@string/app_name"
	... >
	
	<meta-data
		android:name="com.facebook.sdk.ApplicationId"
		android:value="@string/facebook_app_id" />
	<meta-data
		android:name="GoogleAnalyticsId"
		android:value="[GOOGLE_ANALYTICS_ID]" />
	<meta-data
		android:name="com.google.android.gms.version"
		android:value="@integer/google_play_services_version" />
	<meta-data
		android:name="GameTag"
		android:value="[APP_NON_DISPLAY_NAME]" />
	<meta-data
		android:name="IapKey"
		android:value="[IN_APP_PURCHASE_PUBLIC_KEY]" />
	<meta-data
		android:name="AfKey"
		android:value="[APPSFLYER_API_KEY]" />
	... 
```

## Setup values/strings.xml
```xml
<resources>
    <string name="app_name">[GAME_NAME]</string>
    <string name="facebook_app_id">[FACEBOOK_APP_ID]</string>
</resources>
```

# First Java Page (Before Login)
```java
public class MainActivity extends AppCompatActivity {

    private PlaySDKBasic playSDKBasic;
    PostLoginActivity mPostLoginActivity = new PostLoginActivity();

    private Button fbLoginButton;
    private Button fbGoogleButton;
    private Button fbFastButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (playSDKBasic == null) {
            playSDKBasic = PlaySDKBasic.getInstance();
        }

        fbLoginButton = (Button) findViewById(R.id.bFbLogin);
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
				// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
                playSDKBasic.api().loginFacebook(MainActivity.this, mPostLoginActivity);
            }
        });

        fbGoogleButton = (Button) findViewById(R.id.bGoogleLogin);
        fbGoogleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
				// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
                playSDKBasic.api().loginGoogle(MainActivity.this, mPostLoginActivity);
            }
        });

        fbFastButton = (Button) findViewById(R.id.bFastLogin);
        fbFastButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
				// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
                playSDKBasic.api().loginFast(MainActivity.this, mPostLoginActivity);
            }
        });
    }
	...
}
```

### Post Login Activity
Information will be passed to this activity. Here is how to handle it:
```java
public class PostLoginActivity extends AppCompatActivity {

    PlaySDKBasic playSDKBasic = PlaySDKBasic.getInstance();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_login);
		
        Intent intent = getIntent();
        playSDKBasic.postLoginSuccessCheck(intent, new ResponseActivityResult() {
            @Override
            public void onError(int resultCode, String resultMsg) {
                // resultCode: 1000 is success, all other are fails. resultMsg is the success message
            }
            @Override
            public void onSuccess(int resultCode, String resultMsg) {
                // resultCode: all codes other than 1000 are fails. resultMsg describes the problem
            }
        });	
	}
}	
```

You can also check for login success by using `isLoggedIn` & `isLoggedIn`. However, then will not describe any error in detail.
```java
protected void onCreate(Bundle savedInstanceState) {
	...
	playSDKBasic.player().isLoggedIn();	// returns boolean: true or false
	playSDKBasic.player().getLoginType();	// returns String: "google", "facebook", "fast" or "none"
}
```

# After successful, you can initialize GCM, do InApp Purchases & Logout 


## GCM
Obtaining GCM Registration Token, in order to send notifications:
```java
// within onCreate
// 1st parameter: The activity that you are in when you are invoking the GCM init function.
// 2nd parameter: The Activity that launches when GCM notification is pressed
playSDKBasic.gcm().init(PostLoginActivity.this, mGcmLaunchedActivity);

// GCM Registration Token won't be available immediately. So later on, e.g.: in onResume:
playSDKBasic.host().getGcmRegId();	// returns String. This is your GCM Registration Token.
playSDKBasic.host().getGcmStatusMsg();	// returns String. Useful is any error occurs. This will describe what went wrong.
```

### This is how to send GCM Notifications:

1. GCM Registration Token goes under the part labelled "registration_ids" (Highlighted in yellow).

2. Don't forget that the API-Key (Headers -> Authorization -> key) also have to be correct for GCM to work.

![Send GCM Notifications](../Images/GCM_Send.PNG)

For more information about this API-Key, please contact us.


## Payment
This may be an action handler to a "Buy-Button"
```java
playSDKBasic.purchaseControl().purchaseFlow(PostLoginActivity.this, "android.test.purchased", new PurchaseResponseObserver() {
	@Override
	public void onError(String msg, int event) {
		// msg is success message
	}
	@Override
	public void onSuccess(String msg, int event) {
		// msg describes problem
	}
});
```
If you are testing, make sure that you write `playSDKBasic.config().setIsTesting(true);` in the part describe above, under the section: 'First Java Page (Before Login)'.

## Logout
This may be an action handler to a "Logout-Button"
```java
playSDKBasic.api().logout(PostLoginActivity.this);
// Takes only 1 parameter: The activity that you are in when you are invoking the logout function.
// After Logout, you will be directed back to your pre-login activity. It's the activity that you passed in as the 1st parameter into `playSDKBasic.api().startLoginActivity( __, __ )`, which was described above in the section: 'First Java Page (Before Login)'
```
