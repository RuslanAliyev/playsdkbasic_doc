# Purchase Control
Google In App Purchase(IAP)

## Initializing payment:
```java
playSDKBasic.purchaseControl().init(PostLoginActivity.this);
```

## Making a purchase:
```java
playSDKBasic.purchaseControl().purchaseFlow(PostLoginActivity.this, "android.test.purchased", new PurchaseResponseObserver() {
	@Override
	public void onError(String mProductPrice, Purchase purchase) {
		// purchase contains purchase information
	}
	@Override
	public void onSuccess(String mProductPrice, Purchase purchase) {
		// purchase contains purchase information
	}
});
```

## Use the following get methods to retrieve purchase information:

![Purchase Get Methods](../Images/PurchaseGetMethods.PNG)

The useful ones would be `getItemType` and `getSku`.

## The method `toString` will also retrieve a group of useful information:

```
PurchaseInfo(type:inapp):{
	"packageName":"com.forgame.mmxd",
	"orderId":"transactionId.android.test.purchased",
	"productId":"android.test.purchased",
	"developerPayload":"bGoa+V7g\/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ",
	"purchaseTime":0,
	"purchaseState":0,
	"purchaseToken":"inapp:com.forgame.mmxd:android.test.purchased"
}
```