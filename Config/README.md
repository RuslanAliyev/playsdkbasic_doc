# Config

| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| setMobileSystem | String | void | Optional. Default: "2" |
| setGameVersion | String | void | Optional. Default: "" |
| setIsTesting | boolean | void | Optional. Default: true. Affects InApp purchase testing and Loggings |