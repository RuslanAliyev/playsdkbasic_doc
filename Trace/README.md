# Trace

| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| afTrackPurchase |  | Activity mActivity, String productPrice, String productType, String productId  | Using AppsFlyer to track successful purchase |