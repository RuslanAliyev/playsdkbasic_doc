# API

## Google Login:
```java
// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
playSDKBasic.api().loginGoogle(MainActivity.this, mPostLoginActivity);
```

## Facebook Login:
```java
// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
playSDKBasic.api().loginFacebook(MainActivity.this, mPostLoginActivity);
```

## Fast Login:
```java
// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
playSDKBasic.api().loginFast(MainActivity.this, mPostLoginActivity);
```

### For example:
```
fbGoogleButton = (Button) findViewById(R.id.bGoogleLogin);
fbGoogleButton.setOnClickListener(new View.OnClickListener() {
	public void onClick(View v) {
		playSDKBasic.api().loginGoogle(MainActivity.this, mPostLoginActivity);
	}
});
```

## Logout
This may be an action handler to a "Logout-Button"
```java
playSDKBasic.api().logout(PostLoginActivity.this);
// Takes only 1 parameter: The activity that you are in when you are invoking the logout function.
// After Logout, you will be directed back to your pre-login activity. It's the activity that you passed in as the 1st parameter into playSDKBasic.api().startLoginActivity( __, __ ), which was described above in the section: 'First Java Page (Before Login)'
```