# Introduction
| item | description |
| -- | -- |
| Project name | Play SDK Basic |
| Project version | 1.0.0 |
| Release date | 2016-07-19 |
| Developer | Ruslan Aliyev |

# Function lists
- Login 
- Google Analytics
- AppsFlyer
- Google In-app Billing
- Google Cloud Messaging

# Preliminary Setup

#### 1. AppsFlyer

1. Give your project an Application ID, like this: `applicationId "xxx.yyyyy.zz"`. This can be your project's package number. In your application's `build.gradle` file, described below under the heading: **'what build.gradle need to set'**.
2. Contact us, tell us what your Application ID is.
3. We will give you an AppsFlyer key.
4. You put this AppsFlyer key into project, like this: `playSDKBasic.config().setAFKey("AppsFlyer Key")`. In your first `.java` activity's `onCreate` section, described below under the heading: **'First Java Page (Before Login)'**.

#### 2. The aar file

Put playsdkbasic-1.0.0.aar into libs folder of Android Studio Project

# Setup app gradle

### what build.gradle need to set
```gradle
android {
    compileSdkVersion ...
    buildToolsVersion ...
    defaultConfig {
        applicationId "xxx.yyyyy.zz"
		...
	}
	...
}
...
repositories {
    flatDir {
        dirs 'libs'
    }
}
dependencies {
	compile 'com.forgame.playsdkbasic:playsdkbasic-release:1.0.0@aar'
	...
    compile 'com.android.support:appcompat-v7:23.0.0'
    compile 'com.google.android.gms:play-services:8.3.0'
    compile 'com.google.android.gms:play-services-auth:8.3.0'
    compile 'com.mcxiaoke.volley:library:1.0.+@aar' 
    compile 'com.facebook.android:facebook-android-sdk:4.1.0'
}
```

# Setup Manifest

### Permission setup
```xml
<uses-permission android:name="android.permission.VIBRATE" />
<uses-permission android:name="com.android.vending.BILLING" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
<uses-permission android:name="android.permission.AUTHENTICATE_ACCOUNTS" />
<uses-permission android:name="android.permission.WAKE_LOCK" />
<uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="[package name].permission.C2D_MESSAGE" />
<permission android:name="[package name].permission.C2D_MESSAGE"
	android:protectionLevel="signature" />	
```

### Setup MetaData
```xml
<meta-data
  android:name="com.google.android.gms.version"
  android:value="@integer/google_play_services_version"/>
<meta-data
  android:name="com.facebook.sdk.ApplicationId"
  android:value="@string/facebook_app_id"/>
<meta-data
	android:name="GoogleAnalyticsId"
	android:value="@string/google_analytics_id" />
```

** Facebook App ID & Google Analytics ID to `res/values/Strings.xml`**
```xml
...
<string name="facebook_app_id">[Facebook App ID]</string>
<string name="google_analytics_id">[Google Analytics ID]</string>
```

### Manifest's Application Section
add android:name at Application in AndroidManifest.xml and use `com.forgame.playsdkbasic.PlayBasicApplication`
```xml
<application
  android:name="com.forgame.playsdkbasic.PlayBasicApplication"
  android:allowBackup="true"
  android:icon="@drawable/ic_launcher"
  android:label="@string/app_name"
  android:theme="@style/AppTheme" >
  ...
  ...
  ...
</application>
```

### Setup Activity
```xml
<!-- Facebook Login -->
<activity
	android:name="com.facebook.FacebookActivity"
	android:configChanges="keyboard|keyboardHidden|screenLayout|screenSize|orientation"
	android:label="@string/app_name"
	android:theme="@android:style/Theme.Translucent.NoTitleBar" />
<!-- Play SDK Basic Login -->
<activity android:name="com.forgame.playsdkbasic.activity.LoginActivity" />
```

### Setup for Google Cloud Messaging
```xml
<!-- GCM Receiver -->
<receiver
	android:name="com.google.android.gms.gcm.GcmReceiver"
	android:exported="true"
	android:permission="com.google.android.c2dm.permission.SEND">
	<intent-filter>
		<action android:name="com.google.android.c2dm.intent.RECEIVE" />

		<category android:name="com.forgame.playsdkbasic" />
	</intent-filter>
</receiver>
<!-- GCM Receiver Service -->
<service
	android:name="com.forgame.playsdkbasic.util.management.GCMPushReceiverService"
	android:exported="false">
	<intent-filter>
		<action android:name="com.google.android.c2dm.intent.RECEIVE" />
	</intent-filter>
</service>
<!-- GCM Registration Intent Service -->
<service
	android:name="com.forgame.playsdkbasic.util.management.GCMRegistrationIntentService"
	android:exported="false">
	<intent-filter>
		<action android:name="com.google.android.gms.iid.InstanceID" />
	</intent-filter>
</service>
```

### Setup for Google Analytics
```xml
<receiver
	android:name="com.google.android.gms.analytics.AnalyticsReceiver"
	android:enabled="true">
	<intent-filter>
		<action android:name="com.google.android.gms.analytics.ANALYTICS_DISPATCH" />
	</intent-filter>
</receiver>
<service
	android:name="com.google.android.gms.analytics.AnalyticsService"
	android:enabled="true"
	android:exported="false" />
<receiver
	android:name="com.google.android.gms.analytics.CampaignTrackingReceiver"
	android:exported="true">
	<intent-filter>
		<action android:name="com.android.vending.INSTALL_REFERRER" />
	</intent-filter>
</receiver>
<service android:name="com.google.android.gms.analytics.CampaignTrackingService" />
```

### Setup for AppsFlyer
```xml
<receiver
	android:name="com.appsflyer.MultipleInstallBroadcastReceiver"
	android:exported="true">
	<intent-filter>
		<action android:name="com.android.vending.INSTALL_REFERRER" />
	</intent-filter>
</receiver>
```

# First Java Page (Before Login)
```java
public class MainActivity extends AppCompatActivity {

    private PlaySDKBasic playSDKBasic;
    PostLoginActivity mPostLoginActivity = new PostLoginActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (playSDKBasic == null) {
            playSDKBasic = PlaySDKBasic.getInstance();
        }

        playSDKBasic.config().setIsTesting(true);
        playSDKBasic.config().setGameName("abcabc");
        playSDKBasic.config().setGameDisplayName("Abc Abc");
        playSDKBasic.config().setPurchasePublickey("MII.....");
        playSDKBasic.config().setAFKey("af.....");

		// 1st Parameter: This Activity. 2nd Parameter: After Login (success or failure) user will be directed to this activity
        playSDKBasic.api().startLoginActivity(MainActivity.this, mPostLoginActivity);
    }
	...
}
```

### Post Login Activity
Information will be passed to this activity. Here is how to handle it:
```java
public class PostLoginActivity extends AppCompatActivity {

    PlaySDKBasic playSDKBasic = PlaySDKBasic.getInstance();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_login);
		
        Intent intent = getIntent();
        playSDKBasic.postLoginSuccessCheck(intent, new ResponseActivityResult() {
            @Override
            public void onError(int resultCode, String resultMsg) {
                // resultCode: 1000 is success, all other are fails. resultMsg is the success message
            }
            @Override
            public void onSuccess(int resultCode, String resultMsg) {
                // resultCode: all codes other than 1000 are fails. resultMsg describes the problem
            }
        });	
	}
}	
```

You can also check for login success by using `isLoggedIn` & `isLoggedIn`. However, then will not describe any error in detail.
```java
protected void onCreate(Bundle savedInstanceState) {
	...
	playSDKBasic.player().isLoggedIn();	// returns boolean: true or false
	playSDKBasic.player().getLoginType();	// returns String: "google", "facebook", "fast" or "none"
}
```

# After successful, you can initialize GCM, do InApp Purchases & Logout 


## GCM
Obtaining GCM Registration Token, in order to send notifications:
```java
// within onCreate
// 1st parameter: The activity that you are in when you are invoking the GCM init function.
// 2nd parameter: The Activity that launches when GCM notification is pressed
playSDKBasic.gcm().init(PostLoginActivity.this, mGcmLaunchedActivity);

// GCM Registration Token won't be available immediately. So later on, e.g.: in onResume:
playSDKBasic.host().getGcmRegId();	// returns String. This is your GCM Registration Token.
playSDKBasic.host().getGcmStatusMsg();	// returns String. Useful is any error occurs. This will describe what went wrong.
```

### This is how to send GCM Notifications:

1. GCM Registration Token goes under the part labelled "registration_ids" (Highlighted in yellow).

2. Don't forget that the API-Key (Headers -> Authorization -> key) also have to be correct for GCM to work.

![Send GCM Notifications](../Images/GCM_Send.PNG)

For more information about this API-Key, please contact us.


## Payment
This may be an action handler to a "Buy-Button"
```java
playSDKBasic.purchaseControl().purchaseFlow(PostLoginActivity.this, "android.test.purchased", new PurchaseResponseObserver() {
	@Override
	public void onError(String msg, int event) {
		// msg is success message
	}
	@Override
	public void onSuccess(String msg, int event) {
		// msg describes problem
	}
});
```
If you are testing, make sure that you write `playSDKBasic.config().setIsTesting(true);` in the part describe above, under the section: 'First Java Page (Before Login)'.

## Logout
This may be an action handler to a "Logout-Button"
```java
playSDKBasic.api().logout(PostLoginActivity.this);
// Takes only 1 parameter: The activity that you are in when you are invoking the logout function.
// After Logout, you will be directed back to your pre-login activity. It's the activity that you passed in as the 1st parameter into `playSDKBasic.api().startLoginActivity( __, __ )`, which was described above in the section: 'First Java Page (Before Login)'
```
