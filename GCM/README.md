# GCM

Initiate GCM:
```java
playSDKBasic.gcm().init(PostLoginActivity.this, mGcmLaunchedActivity);
// 1st parameter: The activity that you are in when you are invoking the GCM init function.
// 2nd parameter: The Activity that launches when GCM notification is pressed
```