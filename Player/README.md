# Player

| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| getUserID |  | String |  |
| getUserName |  | String |  |
| getUserEmail |  | String |  |
| getFacebookAccessToken | | com.facebook.AccessToken |  |
| getGoogleAccessToken | | String |  |
| isLoggedIn |  | boolean |  |
| getLoginType |  | String | "google", "facebook", "fast" or "none". Default value is "none" |
| getDeviceId |  | String | Android Device Software ID |