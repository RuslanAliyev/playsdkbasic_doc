# Host

| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| getIsTesting |  | boolean | Testing or not |
| getGcmRegId |  | String | Default or if GCM initiation error: "". Use this to send GCM notifications from server |
| getGcmStatusMsg |  | String | Useful for displaying GCM initiation error message |
| getGameVersion |  | String | Default: "" |
| getGameName |  | String | Game's name |
| getGameDisplayName |  | String | Game's name written in a pretty way |
| getPostLoginActivity |  | Activity | The Activity that is redirected to when Login finishes |
| getGcmActivity |  | Activity | The Activity that launches when GCM notification is pressed |
| getPurchasePublickey |  | String | InApp Purchase |
| getAppsFlyerKey |  | String | AppsFlyer |
| getMobileSystem | | String | Default is "2" |